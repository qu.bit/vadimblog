title: Stupid Startup Ideas - 1
author: Rahul Jha
date: 2021-10-12
tags: ssi, startup
url: startup-1.html
summary: We all need Aaloo Chaat in our lives

I am well aware of my incompetence (I have no idea why the autocorrect suggested impotence, I swear) so this entry might end up being the last of its kind. With that said, may I present my maiden startup idea, **Aaloo Chaat**(_applause_).

## The Target Demographic (You)
You do not own a phone. No one does. That is my idea of utopia. If you happen to be one of the snobs who were gifted a phone as soon as they enetered 11th grade, you will do well to beat a hasty retreat (unless you are a cool kid from BITS Pilani, in which case you can read on before investing in the pre-seed round). Your well meaning parents who coerced you to prepare for the JEE also got you a laptop, and using that laptop to connect to the internet is trivial. Even Aslam-the-rickshaw-puller from Abul Fazal is smart enough to download the latest Bhojpuri EDM from pirate-vagene.topsecret-69.com. Because you're reading this, I do not think it is far fetched to assume that your friend circle consists entirely of homo-sapiens of your gender (because come on, no one with a girlfriend is going to read an obscure blog written by a wannabe). Like all sentinent freshmen (an oxymoron), you just _need_ to talk to your friend(s). I know what happened when you did not chat with Srikant from Sarita Vihar last January, the sensex tanked. All of us want the bull run to continue, so you _need_ to talk to your friends. 

## Current Technology (and its limitations)
WhatsApp is not a viable option. Apart from talking about the stock market, Srikant from Sarita Vihar also likes to rate the fairer sex. You sure as hell do not want your mother to find out the rating you gave to Bissmah from Batla House (AAA-. Marginally inferior to Lavanya from Lajpat Nagar, who is an AAA, but thats because you aren't allowed to date a Muslim). You cannot legally restrict your mother from checking _her_ phone, and you are terrible at making the puppy face (seriously, no one likes a puppy with a beard). Erasing the chat history every hour will only cement your father's suspicion that you are a peddler of below-par hashish. Telegram and Signal suffer from the same problems. What do you do? You get the smart friend of yours (who you know is simping over Shagufta from Shaheen Bagh) to build a web app that will allow you to chat with Srikant in incognito mode, leaving no trail whatsoever. 

## The Product
Not everyone has a smart friend, and no you have not disproved the well ordering principle, its just that you have a dumb peer group that went to Sharda University en-masse. Also, Shagufta from Shaheen Bagh ran away with the chap from Zakir Nagar so even if you have a smart friend, you cannot get him to clone facebook for you. No motivation, no crime. That's where we step in. We will build Aaloo Chat, a text only channel of communication. The refuge of all single chaps in India and beyond. I am convinced this will raise a handsome series A, because VCs are stupid enough to fund anything. Will you join me in this adventure, I assure you that I'll do everythng in my power to condemn you to a fate not unlike Eduardo Saverin's.

Send me your CVs (or class 12th marksheet, whichever is more impressive) at kashmir_maange_aazadi@topsecret-69.com (yes, I pirate bhojpuri EDM in my free time).