title: Changing Homes
url: movingtogitlab.html
date: 2021-06-08
author: Rahul Jha
tags: computing, gitlab,
summary: Its always good to paginate stuff

## Abstract (getting old, right ?)
[Github](https://github.com) is a great service, one can host projects, and websites for free. I had 2 websites hosted on github, my personal website and this blog. And while I was perfectly satisfied with the way my website was hosted, logging into Github everytime I added a post to my blog was a massive pain in the [redacted]. And moreover,_juggernautjha_ ,my handle of choice, wasn't available on github, so I decided to switch to Gitlab. In the process, I learnt how to set up CI/CD (two terms that used to frighten the shit out of me as recently as yesterday). And while I don't expect anyone to read this blog, I will chronicle the journey anyway.

## The beginning
So, I was done with my daily quota of Physical chemistry problems from R.C Mukerjee, and for no apparent reason I search Gitlab Pages. And because I am not a savage I searched for setting up SSH on Gitlab (another sad story, all my attempts to set up ssh for my numerous Github accounts have been futile. I can't really blame github for expecting an individual to have just one account). 45 minutes later, I'd SSH set up and to my great pleasure, when I ran _ssh -T git@gitlab.com_, it did say Welcome @juggernautjha. Now came the hard part, setting up CI/CD. It was an experience for me because when the blog was hosted on github, I just used to compile the markdown (i,e generate the site) on my laptop and then upload them (by dragging/dropping, ofc) to my github repository. And again, to my pleasant surprise, setting up CI on gitlab turned out to be as simple as creating a YAML file.

## Pagination
I did not anticipate writing 7 absolutely exquisite pieces of art, so I had nt originally added pagination to my static site generator. But scrolling all the way to read my first blog (the only interesting blog I've written) was tiresome. So I finally added pagination to vadim.

## A propaganda site
I plan on using vadim to start a website that would publish the most disgusting pieces of propaganda, hosted on a github repository because
 <ol>
 <li> I want to see how github actions work </li>
 <li> The twits at IT cell are obviously too dumb to trace the propaganda back to me </li>
 </ol>
## Where are the comments?
For someone like me, whose Gitlab readme says *javascript sucks*, its blasphemy to use javascript for comments. And the website I'd relied upon for comments was *shady* to say the least.

## The future
Now that my static site generator is really a static site generator, using Gitlab's CPU cycles to generate my blog, I am happy and I'll probably try setting up pagination sometime in the near future. <a class = "footnote"> <sup>1</sup><span> Welcome to guinea pig territory</span></a>. As you might've noticed, this blog now supports floating footnotes <a class = "footnote"> <sup>2</sup><span> So be ready to see a shitton of them</span></a>.
