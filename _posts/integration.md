title: Integration!!
url: integration-wee.html
date: 2021-02-02
author: Rahul Jha
tags: math,anecdote,hobby
summary: On my latest hobby : doing tough integrals. And obviously, some ranting. Quite long (by my standards, at least).

## Duh, integration
Like most other Indian high schoolers, I am preparing for the JEE (yeah, **THE JEE**) and it was all but inevitable that I cross paths with tough inetgrals. In the beginning, I passionately hated integration. It was arbitrary according to me (much like semiconductors). Xkcd, naturally, has a comic on it.
![Differentiation vs integration](https://imgs.xkcd.com/comics/differentiation_and_integration.png)
So while differentiation was a piece of cake for me, I struggled with integration. Oftentimes, I spent hours (not exactly hours, but you get the idea, right?) solving a problem only to find out that through some fancy sounding approach ([Dirichlet's Kernel](https://www.wikiwand.com/en/Dirichlet_kernel), anyone), the problem can be done in a few minutes. 
All this depressed me, and I became the 18 year old version of Deepika Padukone. Anyway, we got done with that chapter and I *very convieniently* forgot all about it, busying myself with probability and other nice bits of maths. Now, a few days back, I decided to start revision and guess what's the first topic I took up? Integration.
The monstrocity people lovingly call the FIITJEE GMP was riddled with inetgration sums, and I had to learn integration, properly this time. 

## Integration मधुमक्खी
I get bored pretty easily. So solving the NCERT was completely out of question, and besides, I am way past the phase were I **need** to solve the NCERT. So I started looking for integration contests online, and quickly found out they don't get any better than the [MIT Integration bee](http://www.mit.edu/~pax/integrationbee.html). 45 tough integrals in 20 minutes? Challenge accepted. In the beginning, I struggled terribly. I was able to solve just 4-5 inetgrals in the stipulated time, not because I did not know how to solve them, but because I knew the longer way to solve them. For instance, while integrating sin<sup>m</sup>xcos<sup>n</sup>x , I would use reduction formulae, while the nicer way was to use the [Gamma function](https://www.wikiwand.com/en/Gamma_function). So I started learning these *advanced* things, spending quite a bit of time with my forgotten Arihant book (that *surprisingly* has a section on the gamma function). I also looked up the Feynman trick, only to realise that our teacher had taught this in the class - if only I had paid some attention. I've started doing 3 Inetgration bee qualifiers every week, and since there are only so many qualifiers available online, I've expanded my horizon to the Berkeley Integration bee and the Illinois integration bee. The past few weeks have changed my outlook towards integration, and I've begun to enjoy doing them. My bucket list for college is to start an Integration bee there.

That's all for the week, folks.