url: midnight.html
title: Midnight Musings
date: 2023-07-04
author: Rahul Jha
tags: rant, math, life
summary: On lossy compression, and a questionable work ethic.

## Prologue
What is college if not a series of emails announcing births, deaths, and grades? The kind folks at the placement office were nice enough to send the email announcing the start of the internship season late in the night, ensuring atleast 8 more hours of peace for people who turn off notifications (because their crush is not answering them). 
## Lossy Compression

